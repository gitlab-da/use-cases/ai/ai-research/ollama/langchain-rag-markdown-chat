# Langchain RAG Markdown Chat

Goal: Use a simple knowledge base with Markdown files: https://o11y.love/ or https://opsindev.news/ and ask specific questions about technology topics and learnings. Use the GitLab handbook at https://handbook.gitlab.com/ and research embedding performance optimizations.

Examples shown in @dnsmichi 's QCon London 2024 talk: [Efficient DevSecOps workflows with a little help from AI](https://go.gitlab.com/ZDYNXQ). 

## Scenarios

1. [exploration/](exploration/) contains the learning exploration to get started with loading documents, and web URLs, parse them, create embeddings, store them in a vector database, and invoke the LLM to answer specific questions.
1. [chat/](chat/) implements loading a full knowledge base with embeddings, to answer more sophisticated answers.

## Development

Install Python, e.g. using Homebrew on macOS.

```shell
brew install python
```

Create a virtual environment for Python dependencies, and enable it.

```shell
python3 -m pip install --user virtualenv

python3 -m venv myenv

source myenv/bin/activate 
```

Install the dependencies.

```shell
pip3 install -r requirements.txt 
```

### Usage

Install [Ollama](https://ollama.com/) and pull the Mistral LLM.

```shell
ollama pull mistral
```

#### LLMs

Mixtral requires 26 GB storage and 48 GB RAM. The examples use a smaller model with Mistral in Ollama therefore.

Note: Future examples can work with Anthropic Claude, too.

#### Exploration

```shell
cd exploration/
python3 learning_exploration.py
```

RAG with text embeddings, newsletter question, Ollama Mistral.

![](docs/assets/images/langchain_rag_exploration_ollama_mistral_markdown_newsletter.png)

RAG with web URL embeddings, Rust blog URL, Ollama Mistral.

![](docs/assets/images/langchain_rag_exploration_ollama_mistral_weburl_rust.png)

RAG with web URL embeddings, Rust blog URL, Anthropic Claude 3 (needs source code modifications, and Anthropic API key)

![](docs/assets/images/langchain_rag_exploration_anthropic_claude3_weburl_rust.png)


#### Chat

```shell
cd chat/

# populate the data/ directory with a knowledge base index
# e.g. from https://gitlab.com/everyonecancontribute/observability/o11y.love 
cd data/
git clone https://gitlab.com/everyonecancontribute/observability/o11y.love 

git clone https://gitlab.com/dnsmichi/opsindev.news 

git clone https://gitlab.com/gitlab-com/content-sites/handbook 

cd .. 
```

```shell
python3 rag_chat_kb.py
```

The source code enables opsindev.news and o11y.love questions by default. 

![](docs/assets/images/langchain_rag_chat_prompt_opsindev.news_o11y.love_working.png)

The GitLab handbook is prepared for further testing, and performance optimizations (2000 pages vector db index of 26 MB). LLM answers take a while on a Macbook Pro M1. 

![](docs/assets/images/langchain_rag_chat_prompt_opsindev.news_gitlab_handbook_working.png)

## Learning notes

Follow the learnings from https://dnsmichi.at/2024/01/10/local-ollama-running-mixtral-llm-llama-index-own-tweet-context/ 

1. Something needs to load Markdown files, parse them.
1. How to add embeddings?
1. Which vector database, etc.
1. Can we do that in LangChain as abstraction layer
1. Tools requirement: Ollama, vector db (Qdrant, etc.), service daemons
1. LLM: Use Mixtral as the smaller variant of Mistral 

### Technical analysis

https://python.langchain.com/docs/use_cases/question_answering/quickstart/ 

1. We need a DocumentLoader for Markdown in LangChain, similar to the JSONLoader in earlier attempts with tweets. https://python.langchain.com/docs/modules/data_connection/document_loaders/ 
1. Something needs to parse the Markdown files.
1. Some sort of service worker which does the embeddings and updates.
1. Ollama Chat interface


### LLM providers

- Local: Ollama
- SaaS: Anthropic, etc. (I have access to Anthropic through GitLab, https://docs.gitlab.com/ee/development/ai_features/#anthropic )

#### Embedding

Note: For parsing the data into a vector store, you can use different providers, i.e. OpenAI for Embeddings, and later Anthropic Claude. https://stackoverflow.com/questions/78115486/why-is-it-possible-to-use-openai-embeddings-together-with-anthropic-claude-model 

The OpenAI embedding requires an API key, so lets look for alternative ways, for example, FastEmbed by Qdrant.
https://python.langchain.com/docs/integrations/text_embedding/fastembed/ 

### Vector database

Chroma did not work. Fall back to Qdrant again.

https://python.langchain.com/docs/integrations/vectorstores/qdrant/ 
Adapt to the examples from https://dnsmichi.at/2024/01/10/local-ollama-running-mixtral-llm-llama-index-own-tweet-context/ 


### ConversationalRetrievalChain

https://api.python.langchain.com/en/latest/chains/langchain.chains.conversational_retrieval.base.ConversationalRetrievalChain.html

## Thanks

The following tutorials helped me understand the workflow, and LangChain implementation:

1. https://medium.com/@jerome.o.diaz/langchain-conversational-retrieval-chain-how-does-it-work-bb2d71cbb665 
1. https://medium.com/@dminhk/rag-in-a-box-on-amazon-ec2-using-open-source-part-ii-206dd36edd25 
1. https://betterprogramming.pub/building-a-multi-document-reader-and-chatbot-with-langchain-and-chatgpt-d1864d47e339
1. https://medium.com/@andreanuzzo/chatting-with-your-documentation-building-a-custom-rag-with-langchain-to-enhance-document-70d5d1e4552e 
1. https://medium.com/@onkarmishra/using-langchain-for-question-answering-on-own-data-3af0a82789ed 

They do not use Qdrant as a vector database, nor Ollama. This required additional trial and error, next to learning how chat history works.