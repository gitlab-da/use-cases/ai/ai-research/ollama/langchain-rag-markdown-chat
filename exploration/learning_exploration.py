# Follow the RAG Chat tutorial in https://python.langchain.com/docs/use_cases/question_answering/quickstart/ 
# Modifications made for Ollama and Qdrant as vector store, based on the learnings in https://dnsmichi.at/2024/01/10/local-ollama-running-mixtral-llm-llama-index-own-tweet-context/ 
# Modified Embeddings, to avoid OpenAI API key requirement.  https://python.langchain.com/docs/integrations/text_embedding/fastembed/ 
# Prepared code for Anthropic instead of Ollama. Requires Anthropic API key (commented out)

from langchain import hub

from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_text_splitters import CharacterTextSplitter

# Modified embed
from langchain_community.embeddings.fastembed import FastEmbedEmbeddings

# Modified Vector store
from langchain_community.vectorstores import Qdrant

# Ollama - begin
from langchain_community.llms import Ollama
#from langchain_community.embeddings import OllamaEmbeddings

# Use more lightweight model, mixtral requires 26GB disk and 48 GB RAM
# Required: ollama pull mistral
llm = Ollama(model="mistral")
# Ollama - end 

# Anthropic specific LLM, Claude 3 - begin
#import getpass
#import os 
#os.environ["ANTHROPIC_API_KEY"] = getpass.getpass("Enter your Anthropic API key: ")
#from langchain_anthropic import ChatAnthropic 
#llm = ChatAnthropic(model="claude-3-sonnet-20240229") 
# Anthropic specific LLM, Claude 3 - end 

###################
# Web URL
# Input: None, URL is hardcoded for exploration purposes
# Returns: docs, list of parsed documents 
def load_data_web():
    from langchain_community.document_loaders import WebBaseLoader

    loader = WebBaseLoader(
        web_paths=("https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/",)
    )
    documents = loader.load()

    # Parse 
    from langchain.text_splitter import RecursiveCharacterTextSplitter

    text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=0)
    docs = text_splitter.split_documents(documents)

    return docs     


###################
# Text files 
# Input: None, index.md is hardecoded
# Returns: docs, list of parsed documents 
def load_data_text():
    from langchain_community.document_loaders import TextLoader

    loader = TextLoader("./index.md")
    documents = loader.load()

    # Parse 
    text_splitter = CharacterTextSplitter(chunk_size=500, chunk_overlap=0)
    docs = text_splitter.split_documents(documents)

    return docs 

###################
# Load docs into vector store
# Input: docs, list of parsed documents
# Returns: vectorstore as retriever 
def load_vector_store(docs):
    ###################
    # Vector Store
    # Use FastEmbed here. Or Ollama. 
    embeddings = FastEmbedEmbeddings()

    vectorstore = Qdrant.from_documents (
        docs,
        embeddings,
        path="./qdrant_data",
        collection_name="my_documents"
    )

    # Retrieve and generate using the relevant snippets of the blog.
    retriever = vectorstore.as_retriever()

    return retriever

# RAG prompt
# Input: Question string
# Returns: Response string
def rag_prompt(question):
    prompt = hub.pull("rlm/rag-prompt")

    def format_docs(docs):
        return "\n\n".join(doc.page_content for doc in docs)


    rag_chain = (
        {"context": retriever | format_docs, "question": RunnablePassthrough()}
        | prompt
        | llm
        | StrOutputParser()
    )

    response = rag_chain.invoke(question)

    return response


# TODO: Make this selectable from the CLI 
docs = load_data_web()
#docs = load_data_text()

retriever = load_vector_store(docs)

# web
#response = rag_prompt("How to learn Rust? Summarize the best practices from the article.")

# text 
response = rag_prompt("What is the newsletter about? Add technology insights.")

###################
# Prints 

print(response)

