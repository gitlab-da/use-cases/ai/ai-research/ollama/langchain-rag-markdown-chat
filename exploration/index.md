# opsindev.news 

## 👋 Hey, nice to meet you

From Dev to Ops to DevSecOps to SRE, learning the value of Observability. Interested in solving engineering challenges, and day-2-ops with ML/AI? Welcome to my story; you are invited to learn together :)

I started as a backend developer in an OSS monitoring project, learned how the software was used in Ops, and later appreciated the performance insights and tools to become a better developer. I see myself as the Ops in Dev, or a developer turned ops, with all mistakes on the way. I have adopted cloud-native microservice architectures and set goals to learn new technology (eBPF, OpenTelemetry, AI/ML, etc.), and educate everyone. Join my learning experience by subscribing to the Ops In Dev newsletter! 

## 💡 How to get the newsletter

<!-- Add buttondown iframe -->

<iframe
scrolling="no"
style="width:100%!important;height:220px;border:1px #ccc solid !important"
src="https://buttondown.email/opsindev.news?as_embed=true"
></iframe><br /><br />

### More Options 

- Subscribers will get an email with a [newsletter subscription](https://buttondown.email/opsindev.news/). 
- [opsindev.news](https://opsindev.news/) provides an archive website including search possibilities. 
- You can also subscribe to the [RSS feed](https://buttondown.email/opsindev.news/rss). 

The newsletter is organized in [https://gitlab.com/dnsmichi/opsindev.news/](https://gitlab.com/dnsmichi/opsindev.news/), transparent and public so that everyone can contribute.

If you want to share items for the next newsletter, please check out the [contributing guide](https://opsindev.news/contributing/). Thanks!




<!-- Add disclaimer inspired by https://o11y.news/ -->

> _**Note**_
>
> _This newsletter solely reflects the personal opinion of [Michael Friedrich](https://dnsmichi.at/), and not those of his employer [GitLab](https://about.gitlab.com/)._

