# TODO: Refactor into multiple files and classes

# Load markdown files recursively from a directory tree.
# https://python.langchain.com/docs/modules/data_connection/document_loaders/markdown/

from langchain_community.document_loaders import DirectoryLoader
from langchain_community.document_loaders import UnstructuredMarkdownLoader
from langchain.text_splitter import MarkdownHeaderTextSplitter

# https://python.langchain.com/docs/integrations/text_embedding/fastembed/
from langchain_community.embeddings.fastembed import FastEmbedEmbeddings
from langchain_community.vectorstores.qdrant import Qdrant

# LLM - Ollama
from langchain_community.llms import Ollama
#from langchain_community.embeddings import OllamaEmbeddings

# LangChain ConversationalRetrieval
from langchain.chains import ConversationalRetrievalChain
from langchain.memory import ConversationBufferMemory

import json
import os 

# dump object as json with pretty print
def debug(object):
    print(json.dumps(object, indent=2))

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

class RagChat():
    def __init__(self, directory, glob='./*.md',
                 vector_storage_path='qdrant_data') -> None:
        self.directory = directory
        self.glob = glob
        self.documents = []
        self.embedding = None
        self.vector_storage = None
        self.vector_storage_path = vector_storage_path
        self.docs = []
        self.llm = Ollama(model="mistral") # TODO configurable
        self.qa_chain = None
        self.chat_history = [] # empty, not None
        self.debug = bool(os.environ.get("DEBUG", False))

    def load_documents(self):
        """
        Load markdown files from a directory recursively.
        """
        
        if self.debug:
            debug(self)

        loader = DirectoryLoader(self.directory, glob=self.glob, 
                                 show_progress=True, loader_cls=UnstructuredMarkdownLoader)

        # https://python.langchain.com/docs/modules/data_connection/document_transformers/markdown_header_metadata/
        headers_to_split_on = [
            ("#", "Header 1"),
            ("##", "Header 2"),
            ("###", "Header 3"),
        ]
        splitter = MarkdownHeaderTextSplitter(headers_to_split_on=headers_to_split_on)

        self.documents = []
        for doc in loader.load():
            header_sections = splitter.split_text(doc.page_content)

            if self.debug:
                debug(header_sections)

            self.documents.extend(header_sections)


    def create_embeddings(self):
        self.embedding = FastEmbedEmbeddings()

        if self.debug:
            debug(self.documents)

        self.vector_storage = Qdrant.from_documents(
            documents=self.documents,
            embedding=self.embedding,
            path=self.vector_storage_path,
            collection_name="my_docs"
        )

    def init_chat_data(self):
        retriever = self.vector_storage.as_retriever(search_kwargs=dict(k=3))

        memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True, output_key="answer")

        # https://api.python.langchain.com/en/latest/chains/langchain.chains.conversational_retrieval.base.ConversationalRetrievalChain.html 
        self.qa_chain = ConversationalRetrievalChain.from_llm(
            llm=self.llm,
            chain_type="stuff",
            retriever=retriever,
            return_source_documents=True,
            return_generated_question=True,
            memory=memory,
        )


    def ask(self, query):
        # https://api.python.langchain.com/en/latest/chains/langchain.chains.conversational_retrieval.base.ConversationalRetrievalChain.html

        self.prompt = f"""
            You are an expert in researching in a knowledge base and providing in-depth answers to questions.
            Analyze the provided documents and focus on a summary focussed on engineering knowledge.
            Combine the chat history and follow up question into a standalone question context.
            If you cannot find an answer, encourage the user to rephrase their question and provide more context.
            \n Question: {query}
            \n Chat history: {self.chat_history}
            \n Answer:
        """

        result = self.qa_chain.invoke({"question" : self.prompt, "chat_history": self.chat_history})

        # DEBUG
        if self.debug: 
            debug(str(result))

        # Store the question for the next request 
        self.chat_history.append((query, result['answer']))

        # Return the full result set
        return result


    def clear_history(self):
        self.chat_history = []

# Local utility functions
# TODO - refactor 

# define different qdrant data paths because otherwise they will lock themselves
def setup_data(name, dir) -> RagChat:
    chat = RagChat(dir, "**/*.md", "qdrant_data_" + name)

    chat.load_documents()
    chat.create_embeddings()

    chat.init_chat_data()

    return chat 

def ask_query(chat, query):
    # Check if chat is type of RagChat
    if not isinstance(chat, RagChat):
        raise(TypeError, "chat must be of type RagChat")

    return chat.ask(query)

def print_answer(name, result):
    yellow = "\033[0;33m"
    green = "\033[0;32m"
    white = "\033[0;39m"

    answer = result["answer"] if "answer" in result else ""
    question = result["generated_question"] if "generated_question" in result else ""

    print(f"{yellow}Question: \n{white}" + question)
    print(f"{green}Answer: \n{white}" + answer)
    write_result_to_file(answer, name)

def write_result_to_file(result, file_name): 
    with open(file_name + "_result.json", 'w+') as f: 
        json.dump(result, f, indent=2)

def main():
    # Newsletter 
    name = "opsindev.news"
    chat = setup_data(name, dir="data/opsindev.news/docs")

    result = ask_query(chat, "Identify and summarize the focus on the Ops in Dev newsletter. Share technologies and practices for observability, engineering challenges, ML/AI, and cloud-native microservices.")
    print_answer(name, result)

    # o11y.love 
    name = "o11y.love"
    chat = setup_data(name, dir="data/o11y.love/docs")

    result = ask_query(chat, "Please explain eBPF and how it helps Observability. Focus on the learning path, and tool recommendations.")
    print_answer(name, result)

    # Handbook
    name = "handbook"
    chat = setup_data(name, dir="data/handbook/content")
    result = ask_query(chat, "What is low-context communication and how does it help in all-remote work environments?")
    print_answer(name, result)
    

if __name__ == "__main__":
    main()